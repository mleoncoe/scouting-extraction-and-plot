//#include "trees.h"
#include "histograms.h"
#include "TMatrixDSym.h"
#include "TFitResult.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"


using namespace std;

ofstream out;
void plot_orbit_muons(Int_t orbit_number[], Int_t n_muons[], Int_t total_entrances);
void plot_orbit_muons2(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, string output_dir);
void plot_orbit_muons_hfoc(float orbit_number[], float n_muons[], Int_t total_entrances, string output_dir);
void plot_orbit_muons_scouting(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir);
void plot_residuals(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, string output_dir);
void plot_residuals_cut(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, string output_dir);
void plot_scouting_v_hfoc(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir);
void plot_nfit(float orbit_number[], float n_muons[], Int_t total_entrances);
std::vector<float> plot_nfit_vector(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir);
std::vector<float> plot_nfit_vector_extended(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir);


std::vector< std::vector<int> >  read_csv(std::string filename){
    // Reads a CSV file of columns of ints

    // Create a vector of <int vector> to store the result
    std::vector< std::vector<int> > result;

    // Create an input filestream
    std::ifstream myFile(filename);

    // Make sure the file is open
    if(!myFile.is_open()) throw std::runtime_error("Could not open file");

    // Helper vars
    std::string line, colname;
    int val;
    // Read data, line by line
    while(std::getline(myFile, line))
    {
        // Create a stringstream of the current line
        std::stringstream ss(line);
        
        // Keep track of the current column index
        int colIdx = 0;
        vector<int> v1;
        
        // Extract each integer
        while(ss >> val){         
            v1.push_back(val);
            // If the next token is a comma, ignore it and move on
            if(ss.peek() == ',') ss.ignore();
            //v1.push_back(val);
            // Increment the column index
            colIdx++;
        }
        result.push_back(v1);
    }

    // Close file
    myFile.close();

    return result;
}


void write_csv(float ls_array[], float hfoc_array[], float scouting_array[], int total_entrances, string filepath){
    std::ofstream myfile;
    myfile.open (filepath);
    //myfile << "This is the first cell in the first column.\n";
    for (int i = 0 ; i < total_entrances ; i = i +1 ){
        myfile << ls_array[i] <<","<< hfoc_array[i]<<","<< scouting_array[i]<<"\n";
    }
    myfile.close();
}


//////////////////////////////////////////////////////////
//////////////GLOBAL VARIABLES////////////////////////////
//////////////////////////////////////////////////////////

string run_number;
string fill_number;
//float A = 0.1;
//float r = 0.01;  
//Double_t myFunc(double x) { return (1/2)*(exp(x)*(A*sin(r*x))); }


int main(int argc, char* argv[]) {

    //////////////////////////////////////////////////////////
    //////////////GET EXECUTION ARGUMENTS/////////////////////
    //////////////////////////////////////////////////////////

    if (argc <= 2){
		// On some operating systems, argv[0] can end up as an empty string instead of the program's name.
		// We'll conditionalize our response on whether argv[0] is empty or not.
		if (argv[0])
			std::cout << "Usage: " << argv[0] << " <number>"<< " <number>" << '\n';
		else
			std::cout << "Usage: <program name> <number> <number>" << '\n';
		return 1;
	}

	std::stringstream convert{ argv[1] }; // set up a stringstream variable named convert, initialized with the input from argv[1]
    std::stringstream convert2{ argv[2] };
	int myint{};
    int myFill{};
	if (!(convert >> myint)) // do the conversion
		myint = 0; // if conversion fails, set myint to a default value
    if (!(convert2 >> myFill)) // do the conversion
		myFill = 0; // if conversion fails, set myint to a default value

	std::cout << "Run to process: " << myint << '\n';
    std::cout << "Fill: " << myFill << '\n';

    //////////////////////////////////////////////////////////
    //////////////DEFINITION OF CONSTANTS/////////////////////
    //////////////////////////////////////////////////////////

    int orbits_per_lumiSection= pow(2,18);
    int lumiSection= 262144;
    float pt_threshold = 2.5;
    float bx_value = 100;
    float time_orbit = 88.92*pow(10,-6);
    cout << " time_orbit = " << time_orbit << endl;
    //float time_lumisection = 23.3;
    float time_lumisection = time_orbit*pow(2,18);
    cout << " time lumisection = " << time_lumisection << endl;

    //////////////////////////////////////////////////////////
    //////////////DEFINITION OF PATHS/////////////////////////
    //////////////////////////////////////////////////////////

    ///////////Pt cut to string, and repacing . with _
    std::stringstream pt_threshold_ss;
    pt_threshold_ss << std::fixed << std::setprecision(1) << pt_threshold;
    string pt_threshold_str = pt_threshold_ss.str();
    std::replace(pt_threshold_str.begin(), pt_threshold_str.end(), '.', '_');
    
    //Paths
    run_number = to_string(myint);
    fill_number = to_string(myFill);

    bool lxplus = true;
    bool bx_analysis = false;
    string dir_output_plots;
    string file_scouting_base;
    string directorio_rate_root;
    string filepath_csv;

    if(lxplus && bx_analysis) {
        dir_output_plots = "/eos/user/m/mleoncoe/www/ScoutingPlots/EMTF/Scouting_plots_"+run_number+"_Ptmin_"+pt_threshold_str+"_bx_100";
        file_scouting_base = "/eos/user/m/mleoncoe/Scouting_EMTF_"+run_number+"/processed_run_"+run_number+"_variables_test_";
        directorio_rate_root = "./datos/mleoncoe_bril_"+fill_number+"_"+run_number+".root";
        filepath_csv = "/eos/user/m/mleoncoe/www/ScoutingPlots/EMTF/Scouting_plots_"+run_number+"_Ptmin_"+pt_threshold_str+"_bx_100"+"/scout_hfoc_ls_data_"+fill_number+"_"+run_number+"_Ptmin_"+pt_threshold_str+"_bx_100"+".csv";
    } else if(lxplus){
        dir_output_plots = "/eos/user/m/mleoncoe/www/ScoutingPlots/EMTF/Scouting_plots_"+run_number+"_Ptmin_"+pt_threshold_str;
        file_scouting_base = "/eos/user/m/mleoncoe/Scouting_EMTF_"+run_number+"/processed_run_"+run_number+"_variables_test_";
        directorio_rate_root = "./datos/mleoncoe_bril_"+fill_number+"_"+run_number+".root";
        filepath_csv = "/eos/user/m/mleoncoe/www/ScoutingPlots/EMTF/Scouting_plots_"+run_number+"_Ptmin_"+pt_threshold_str+"/scout_hfoc_ls_data_"+fill_number+"_"+run_number+"_Ptmin_"+pt_threshold_str+".csv";
    } else{
        dir_output_plots = "./Plots";
        file_scouting_base = "./datos/processed_run_"+run_number+"_variables_test_";
        directorio_rate_root = "./datos/mleoncoe_bril_7333_"+run_number+".root";
        filepath_csv = "./scout_hfoc_ls_data_"+fill_number+"_"+run_number+"_Ptmin_"+pt_threshold_str+".csv";
    }

    //string dir_EMTF = "/eos/user/m/mleoncoe/www/ScoutingPlots/EMTF/";
    //gSystem->mkdir(dir_EMTF.c_str(),kFALSE);
    gSystem->mkdir(dir_output_plots.c_str(),kFALSE);

    //////////////////////////////////////////////////////////
    //////////////HFOC READ INFO FROM ROOT FILE///////////////
    //////////////////////////////////////////////////////////

    cout << " leyendo tree hfoc ..." << endl;

    TFile* file = TFile::Open(directorio_rate_root.c_str());
    TTreeReader myReader("lumi", file);

    TTreeReaderValue<float> ls_rate(myReader, "ls");
    //TTreeReaderValue<float> bx_rate(myReader, "bx");
    TTreeReaderValue<float> hfoc_rate(myReader, "hfoc");

    std::vector<float> ls_rate_v;
    std::vector<float> ls_rate_v_copy;
    //std::vector<float> bx_rate_V;
    std::vector<float> hfoc_rate_v;
    cout << " poblando vectores ..." << endl;
    int n_entrances_tree = 0;

    while (myReader.Next()) {
        //hist->Fill(*ls_rate);
        ls_rate_v.push_back(*ls_rate);
        ls_rate_v_copy.push_back(*ls_rate);
        //bx_rate_V.push_back(*bx_rate);
        hfoc_rate_v.push_back(*hfoc_rate);
        //cout << " ls_rate = " << *ls_rate << endl;
        n_entrances_tree++;
    }

    std::vector<float> ls_values_v;
    sort(ls_rate_v_copy.begin(), ls_rate_v_copy.end());
    
    float value_v = 0;
    for (int i = 0 ; i < n_entrances_tree ; i = i +1 ){
        value_v = ls_rate_v_copy[i];
        if( value_v != ls_rate_v_copy[i-1] && i!=0){
            ls_values_v.push_back(value_v);
        }
    }

    cout << " llenando arrays..." << endl;

    int num_ls_values = ls_values_v.size();
    float* hfoc_array = new float[num_ls_values];
    float* ls_array = new float[num_ls_values];

    for (int i = 0 ; i < num_ls_values ; i = i +1 ){
        float sum_hfoc = 0;
        for (int n = 0 ; n < n_entrances_tree ; n = n +1 ){
            float ls_val = ls_rate_v[n];
            float hfoc_val = hfoc_rate_v[n];
            if( ls_val == ls_values_v[i]){
                sum_hfoc = sum_hfoc + hfoc_val;
            }
        }
        hfoc_array[i]  = sum_hfoc;
        ls_array[i] = ls_values_v[i];
    }
    /*
    float init_lumi = 0.863;
    
    if(myint == 325117){
        init_lumi = 0.863;
    } else if(myint == 325097){
        init_lumi = 1.928;
    } else if(myint == 325099){
        init_lumi = 1.783;
    } else if(myint == 325022){
        init_lumi = 1.890;
    } else if(myint == 325159){
        init_lumi = 1.478;
    } else if(myint == 325174){
        init_lumi = 0.556;
    }

    for (int i = 1 ; i < num_ls_values ; i = i +1 ){
        hfoc_array[i]  = hfoc_array[i]*init_lumi/hfoc_array[0];
    }
    std::cout << std::fixed << std::setprecision(16);
    cout << "Factor = " << init_lumi/hfoc_array[0] << endl;

    hfoc_array[0] = init_lumi;
    */
    for (int i = 0 ; i < num_ls_values ; i = i +1 ){
        hfoc_array[i]  = hfoc_array[i]*0.000006758933;
    }
    
    cout << "ploteando..." << endl;

    //plot_hist(hist,"lumi_rate", "Lumisection", "none", "","",0);
    //plot_hist(hist,"lumi_rate", "Lumisection", "none", "","",0);

    plot_orbit_muons_hfoc(ls_array, hfoc_array, num_ls_values, dir_output_plots);
    plot_orbit_muons_hfoc(ls_array, hfoc_array, num_ls_values, dir_output_plots);

    //////////////////////////////////////////////////////////
    //////////////SCOUTING FILES//////////////////////////////
    //////////////////////////////////////////////////////////

    //int num_files_scouting = 52;
    int num_files_scouting = 52;
    

    //std::vector<float> muon_orbit_i;
    //std::vector<float> muon_;
    //std::vector<float> hfoc_rate_v;

    int total_muons = 0;
    
    int ls_counter = 0;
    int muon_counter = 0;
    int orbit_counter_ini = 0;
    int orbit_counter = 0;
    //int initial_ls = 27;
    int initial_ls = 0;
    int* n_muons_scout_ = new int[10000]; 
    int* ls_scout_ = new int[10000];


    for (int i=0; i<num_files_scouting; i = i+1){

        cout << "leyendo archivo scouting root numero " << i << endl;
        
        string file_scouting_i = file_scouting_base+to_string(i)+"_"+to_string(i)+".root";
        TFile* file_sc_i = TFile::Open(file_scouting_i.c_str());
        TTreeReader myReader2("tree", file_sc_i); ///MODIFICAR

        TTreeReaderValue<float> orbit_tree(myReader2, "orbit_tree");
        TTreeReaderValue<float> pt_tree(myReader2, "pt");
        TTreeReaderValue<float> bx_tree(myReader2, "bx");

        TTreeReaderValue<float> quality(myReader2, "quality");

        cout << " poblando vectores ..." << endl;
        int n_entrances_tree = 0;

        while (myReader2.Next()) {

            int quality_ = (int) *quality;

            //DEBUG 
            /*if( (quality_==12) || (quality_==8) || (quality_==4) ){
                //cout << " Different quality = " << quality_ << endl;
            } else{
                cout << " Different quality = " << quality_ << endl;
            } */

            if(true /*(int) *quality==12*/ ){
                //Set on bx  analysis if bx_analysis is set to true
                bool bx_tree_bool;
                if(bx_analysis){
                    if((int) *bx_tree==bx_value){
                        bx_tree_bool = true;
                    } else {
                        bx_tree_bool = false;
                    }
                } else{
                    bx_tree_bool = true;
                }

                if(*pt_tree>=pt_threshold &&  bx_tree_bool){

                    if(total_muons==0){
                        orbit_counter_ini = (int) *orbit_tree;
                        initial_ls = orbit_counter_ini/orbits_per_lumiSection;
                        cout << " 2.0 ..." << endl;
                    }
                    //hist->Fill(*ls_rate);
                    //muon_orbit_i.push_back(*orbit_tree);
                    //cout << " ls_rate = " << *ls_rate << endl;
                    total_muons++;

                    orbit_counter = (int) *orbit_tree;
                    int orbit_diff = orbit_counter-orbit_counter_ini;

                    muon_counter++;

                    //DEBUG
                    if( orbit_diff>= (orbits_per_lumiSection*1.1) ){
                        cout << " Big jump " << endl;
                        cout << " Different quality = " << quality_ << endl;
                    }

                    if (orbit_diff>=orbits_per_lumiSection){

                        //cout << " 2.1 ..." << endl;
                        //cout << " muon_counter = " <<muon_counter<< endl;
                        //cout << " ls_counter+1 = " <<ls_counter+1<< endl;

                        n_muons_scout_[ls_counter] = muon_counter;
                        ls_scout_[ls_counter] = initial_ls+ls_counter+1;
                        //ls_scout_[ls_counter] = initial_ls+ls_counter;

                        ls_counter++;
                        muon_counter = 0;

                        orbit_counter_ini = (int) *orbit_tree;

                        //DEBUG
                        cout << " ls =  " << ls_counter<< endl;
                        cout << " last muon quality_ = " << quality_ << endl;
                        float bx_ = *bx_tree;
                        float pt_ = *pt_tree;
                        cout << " last muon bx_ = " << bx_ << endl;
                        cout << " last muon pt_ = " << pt_ << endl;
                    }
                } else {
                    //cout << "hola locote" << endl;
                }
            } 
        }
    }
    cout << "---------------------------------------------------" << endl;
    cout << " initial_ls =" << initial_ls << endl;
    cout << "---------------------------------------------------" << endl;
    cout << " 3 ..." << endl;
    int n_entrances_scouting = ls_counter;
    cout << " 4 ..." << endl;

    cout << " n_entrances_scouting = " <<n_entrances_scouting<< endl;

    //////////plot_orbit_muons(ls_scout_, n_muons_scout_, n_entrances_scouting);
    //////////plot_orbit_muons(ls_scout_, n_muons_scout_, n_entrances_scouting);

    float* rate = new float[n_entrances_scouting];
    float* lumis_number_float = new float[n_entrances_scouting];

    for (int k=0; k<n_entrances_scouting; k = k+1){
        lumis_number_float[k] = (float) ls_scout_[k];
        rate[k] = n_muons_scout_[k]/time_lumisection;
        //cout << " lumis_number_float[k] = " <<lumis_number_float[k]<< endl;
        //cout << " rate[k] = " <<rate[k]<< endl;
        
    }

    plot_orbit_muons_scouting(lumis_number_float, rate, n_entrances_scouting,"lumiSection_rate_scouting", false, dir_output_plots);
    plot_orbit_muons_scouting(lumis_number_float, rate, n_entrances_scouting,"lumiSection_rate_scouting", false, dir_output_plots);

    cout << " 5 ..." << endl;

    //////////////////////////////////////////////////////////
    ////HFOC VALUES CUTING INTO SCOUTING DATASET SIZE/////////
    //////////////////////////////////////////////////////////

    int lumis_entrances = n_entrances_scouting;

    cout << " buscando minimos ... " << endl;

    //Find minimum and max of scouting array

    int min_lumi_scouting = lumis_number_float[0];
    int max_lumi_scouting = lumis_number_float[n_entrances_scouting-1];
    cout << "---------------------------------------------------" << endl;
    cout << " max_lumi_scouting= " << max_lumi_scouting<< endl;
    cout << " min_lumi_scouting = " << min_lumi_scouting << endl;
    cout << " max_lumi_scouting-min_lumi_scouting +1= " << max_lumi_scouting-min_lumi_scouting +1<< endl;
    cout << " n_entrances_scouting = " << n_entrances_scouting << endl;
    cout << "---------------------------------------------------" << endl;

    float* ls_hfoc_cut = new float[max_lumi_scouting-min_lumi_scouting+1];
    float* lumi_hfoc_cut = new float[max_lumi_scouting-min_lumi_scouting+1];
    int entrances_compatible_hfoc = 0;

    for (int i=0; i<num_ls_values; i++) {
        if (ls_array[i] >= min_lumi_scouting && ls_array[i] <= max_lumi_scouting){
            lumi_hfoc_cut[entrances_compatible_hfoc] = ls_array[i+1];
            ls_hfoc_cut[entrances_compatible_hfoc] = hfoc_array[i+1];
            //lumi_hfoc_cut[entrances_compatible_hfoc] = ls_array[i];
            //ls_hfoc_cut[entrances_compatible_hfoc] = hfoc_array[i];
            
            //cout << " /////////////////// "<< endl;
            //cout << " lumi_hfoc_cut[entrances_compatible_hfoc] = " << lumi_hfoc_cut[entrances_compatible_hfoc] << endl;
            //cout << " ls_hfoc_cut[entrances_compatible_hfoc] = " << ls_hfoc_cut[entrances_compatible_hfoc] <<endl;
            //cout << " /////////////////// "<< endl;
            entrances_compatible_hfoc++;
        } 
    }

    cout << " entrances_compatible_hfoc = " << entrances_compatible_hfoc << endl;
    cout << " n_entrances_scouting = " << n_entrances_scouting << endl;
    cout << " max_lumi_scouting-min_lumi_scouting = " << max_lumi_scouting-min_lumi_scouting << endl;
     // /*
    plot_orbit_muons2(lumi_hfoc_cut, ls_hfoc_cut, entrances_compatible_hfoc, "lumiSection_rate_hfoc_cut", dir_output_plots);
    plot_orbit_muons2(lumi_hfoc_cut, ls_hfoc_cut, entrances_compatible_hfoc, "lumiSection_rate_hfoc_cut", dir_output_plots);

    //////////////////////////////////////////////////////////
    //////HFOC VS SCOUTING RATE PLOTING AND FIT///////////////
    //////////////////////////////////////////////////////////

    plot_scouting_v_hfoc(ls_hfoc_cut, rate, lumis_entrances, "scouting_v_hfoc", false, dir_output_plots);
    plot_scouting_v_hfoc(ls_hfoc_cut, rate, lumis_entrances, "scouting_v_hfoc", false, dir_output_plots);
    // */
    //plot_nfit(ls_hfoc_cut, rate, lumis_entrances);
    //plot_nfit(ls_hfoc_cut, rate, lumis_entrances);

    std::vector<float> fit_results_vector;
    std::vector<float> fit_results_vector_extended;

    fit_results_vector = plot_nfit_vector(ls_hfoc_cut, rate, lumis_entrances, "lumi_rate_fitted",false, dir_output_plots);
    fit_results_vector = plot_nfit_vector(ls_hfoc_cut, rate, lumis_entrances, "lumi_rate_fitted",false, dir_output_plots);
    fit_results_vector_extended = plot_nfit_vector_extended(ls_hfoc_cut, rate, lumis_entrances, "lumi_rate_fitted_extended",false, dir_output_plots);
    fit_results_vector_extended = plot_nfit_vector_extended(ls_hfoc_cut, rate, lumis_entrances, "lumi_rate_fitted_extended",false, dir_output_plots);
    
    cout << " fit_results_vector[0] = " << fit_results_vector[0] <<endl;
    cout << " fit_results_vector[1] = " << fit_results_vector[1] <<endl;
    cout << " fit_results_vector[2] = " << fit_results_vector[2] <<endl;
    cout << " fit_results_vector[3] = " << fit_results_vector[3] <<endl;

    float* residuals = new float[lumis_entrances];

    for (int i=0; i<lumis_entrances; i++) {

        float predicted_fit_val = fit_results_vector[2]*ls_hfoc_cut[i]+fit_results_vector[0];
        float difference = rate[i]-predicted_fit_val;
        float diff_entre_pred = difference/predicted_fit_val;
        float log_diff_entre_pred = log(diff_entre_pred);
        float log_sqrt = log ( sqrt( pow(difference,2) ) );
        float sqrt_ = sqrt( pow(difference,2) ) ;
        //residuals[i] = pow(difference,2);
        residuals[i] = diff_entre_pred;
    }

    float min_x = 0.6;
    int count_res = 0;
    std::vector<float> lumi_res;
    std::vector<float> ls_res;
    /*for (int i=0; i<lumis_entrances; i++) {
        if (ls_hfoc_cut[i]>min_x){
            ls_res.push_back(ls_hfoc_cut[i]);
            lumi_res.push_back(residuals[i]);
            count_res++;
        }
    }*/

    for (int i=0; i<lumis_entrances; i++) {
        ls_res.push_back(ls_hfoc_cut[i]);
        lumi_res.push_back(residuals[i]);
        count_res++;
    }

    float* lumi_res_a = new float[count_res];
    float* ls_res_a = new float[count_res];
    for (int i=0; i<count_res; i++) {
        lumi_res_a[i] = lumi_res[i]*100;
        ls_res_a[i] = ls_res[i];
    }
    // /*
    plot_residuals(ls_res_a, lumi_res_a, count_res, "residual_fit", dir_output_plots);
    plot_residuals(ls_res_a, lumi_res_a, count_res, "residual_fit", dir_output_plots);
    plot_residuals_cut(ls_res_a, lumi_res_a, count_res,"residual_fit_cut", dir_output_plots);
    plot_residuals_cut(ls_res_a, lumi_res_a, count_res,"residual_fit_cut", dir_output_plots);
    // */


    /////////////////////////////////////
    //////EXCLUDE OUTLIERS AND SCAN//////
    /////////////////////////////////////

    int lumi_outlier = -1;
    int scan_begin = -1;
    int scan_end = -1;
    if(myint == 325117){
        lumi_outlier = 420;
        scan_begin = 495;
        scan_end = 520;
    } else if(myint == 325097){
        lumi_outlier = -1;
        scan_begin = 40;
        scan_end = 56;
    } else if(myint == 325022){
        lumi_outlier = -1;
        scan_begin = 0;
        scan_end = 60;
    } else if(myint == 325099){
        lumi_outlier = -1;
        scan_begin = 138;
        scan_end = 150;
    } else if(myint == 325159){
        lumi_outlier = -1;
        scan_begin = 0;
        scan_end = 65;
    } else if(myint == 325174){
        lumi_outlier = -1;
        scan_begin = 26;
        scan_end = 41;
    }
    
    int clean_entrances = max_lumi_scouting-min_lumi_scouting+1-3;
    float* ls_hfoc_clean = new float[clean_entrances];
    float* lumi_hfoc_clean = new float[clean_entrances];

    float* lumis_number_clean = new float[clean_entrances];
    float* rate_clean = new float[clean_entrances];

    int entrances_clean = 0;
    //int lumi_outlier = 420;
    
    for (int i=0; i<num_ls_values; i++) {
        if (ls_array[i] > min_lumi_scouting && 
        ls_array[i] < max_lumi_scouting &&
        ls_array[i] != lumi_outlier &&
        (ls_array[i] > scan_end  ||
        ls_array[i] < scan_begin) ){

            lumi_hfoc_clean[entrances_clean] = ls_array[i+1];
            ls_hfoc_clean[entrances_clean] = hfoc_array[i+1];
            lumis_number_clean[entrances_clean] = lumis_number_float[i+1];
            rate_clean[entrances_clean] = rate[i+1];
            //lumi_hfoc_clean[entrances_clean] = ls_array[i];
            //ls_hfoc_clean[entrances_clean] = hfoc_array[i];
            //lumis_number_clean[entrances_clean] = lumis_number_float[i];
            //rate_clean[entrances_clean] = rate[i];
            entrances_clean++;
            //cout << " ls_array[i] = " << ls_array[i] << endl;
        } 
    }
    int entrances_clean_ = 0;
    for (int i=0; i<n_entrances_scouting; i++) {
        if (lumis_number_float[i] > min_lumi_scouting && 
        lumis_number_float[i] < max_lumi_scouting &&
        lumis_number_float[i] != lumi_outlier &&
        (lumis_number_float[i] > scan_end  ||
        lumis_number_float[i] < scan_begin) ){
            lumis_number_clean[entrances_clean_] = lumis_number_float[i];
            rate_clean[entrances_clean_] = rate[i]/1000; /////ENTRE MIL PA QUE SEAN kHz
            //rate_clean[entrances_clean_] = rate[i]; 
            entrances_clean_++;
            //cout << " ls_array[i] = " << ls_array[i] << endl;
        } 
    }

    cout << " entrances_clean = " << entrances_clean << endl;
    cout << " entrances_clean_ = " << entrances_clean_ << endl;
    cout << " clean_entrances = " << clean_entrances << endl;
    cout << " max_lumi_scouting-min_lumi_scouting+1-3 = " << max_lumi_scouting-min_lumi_scouting+1-3 << endl;

    plot_orbit_muons_scouting(lumis_number_clean, rate_clean, entrances_clean,"lumiSection_rate_scouting_clean", true, dir_output_plots);
    plot_orbit_muons_scouting(lumis_number_clean, rate_clean, entrances_clean,"lumiSection_rate_scouting_clean", true, dir_output_plots);
    
    plot_orbit_muons2(lumi_hfoc_clean, ls_hfoc_clean, entrances_clean, "lumiSection_rate_hfoc_cut_clean", dir_output_plots);
    plot_orbit_muons2(lumi_hfoc_clean, ls_hfoc_clean, entrances_clean, "lumiSection_rate_hfoc_cut_clean", dir_output_plots);
    
    plot_scouting_v_hfoc(ls_hfoc_clean, rate_clean, entrances_clean_, "scouting_v_hfoc_clean", true, dir_output_plots);
    plot_scouting_v_hfoc(ls_hfoc_clean, rate_clean, entrances_clean_, "scouting_v_hfoc_clean", true, dir_output_plots);

    write_csv(lumis_number_clean, ls_hfoc_clean, rate_clean, entrances_clean_, filepath_csv);

    std::vector<float> fit_results_vector_;
    std::vector<float> fit_results_vector_extended_;

    fit_results_vector_ = plot_nfit_vector(ls_hfoc_clean, rate_clean, entrances_clean, "lumi_rate_fitted_clean", true, dir_output_plots);
    fit_results_vector_ = plot_nfit_vector(ls_hfoc_clean, rate_clean, entrances_clean, "lumi_rate_fitted_clean", true, dir_output_plots);
    
    fit_results_vector_extended_ = plot_nfit_vector_extended(ls_hfoc_clean, rate_clean, entrances_clean, "lumi_rate_fitted_extended_clean", true, dir_output_plots);
    fit_results_vector_extended_ = plot_nfit_vector_extended(ls_hfoc_clean, rate_clean, entrances_clean, "lumi_rate_fitted_extended_clean", true, dir_output_plots);

    cout << " fit_results_vector[0] = " << fit_results_vector_[0] <<endl;
    cout << " fit_results_vector[1] = " << fit_results_vector_[1] <<endl;
    cout << " fit_results_vector[2] = " << fit_results_vector_[2] <<endl;
    cout << " fit_results_vector[3] = " << fit_results_vector_[3] <<endl;
    
    float* residuals_ = new float[entrances_clean];
    for (int i=0; i<entrances_clean; i++) {
        float predicted_fit_val = fit_results_vector_[2]*ls_hfoc_clean[i]+fit_results_vector_[0];
        float difference = rate_clean[i]-predicted_fit_val;
        float diff_entre_pred = difference/predicted_fit_val;
        residuals_[i] = diff_entre_pred;
    }


    float min_x_ = 0.6;
    int count_res_ = 0;
    std::vector<float> lumi_res_;
    std::vector<float> ls_res_;
    /*for (int i=0; i<entrances_clean; i++) {
        if (ls_hfoc_clean[i]>min_x_){
            ls_res_.push_back(ls_hfoc_clean[i]);
            lumi_res_.push_back(residuals_[i]);
            count_res_++;
        }
    }*/

    for (int i=0; i<entrances_clean; i++) {
        ls_res_.push_back(ls_hfoc_clean[i]);
        lumi_res_.push_back(residuals_[i]);
        count_res_++;
    }
    float* lumi_res_a_ = new float[count_res_];
    float* ls_res_a_ = new float[count_res_];
    for (int i=0; i<count_res_; i++) {
        ls_res_a_[i] = ls_res_[i];
        lumi_res_a_[i] = lumi_res_[i]*100;
    }
    plot_residuals_cut(ls_res_a_, lumi_res_a_, count_res_,"residual_fit_cut_clean", dir_output_plots);
    plot_residuals_cut(ls_res_a_, lumi_res_a_, count_res_,"residual_fit_cut_clean", dir_output_plots);
    //plot_residuals_cut(ls_res_a_, lumi_res_a_, count_res_,"residual_fit_cut_clean");
    //plot_residuals_cut(ls_res_a_, lumi_res_a_, count_res_,"residual_fit_cut_clean");
    //plot_residuals(ls_hfoc_clean, residuals_, clean_entrances,"residual_fit_cut_clean");
    //plot_residuals(ls_hfoc_clean, residuals_, clean_entrances,"residual_fit_cut_clean");
    //*/

    //delete[] orbit_number;
    //delete[] n_muons;
    //delete[] lumis_number_a;
    //delete[] n_muons_lumis_a;
    //delete[] n_muons_first_a;
    delete[] residuals;

    //delete[] lumis_number_centered;
    delete[] ls_hfoc_cut;
    delete[] lumi_hfoc_cut;
    
    delete[] hfoc_array;
    delete[] ls_array;
    //*/
}

void plot_orbit_muons(Int_t orbit_number[], Int_t n_muons[], Int_t total_entrances){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    g->GetYaxis()->SetTitle("Number of muons");
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("Lumi section ([2^{18}] Orbits)");
    g->GetXaxis()->SetTitleOffset(1);
    
    g->SetMinimum(0.);

    g->Draw("ap");


    //c0->SetLogx();
    //c0->SetLogy();
    /*
    string tex_string_02 = "num points =  "+to_string(total_entrances);
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.25, 0.854, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();
    */
    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    c0->SaveAs("./Plots/lumiSection_nmuons.pdf");
    c0->SaveAs("./Plots/lumiSection_nmuons.png");

    //Delete all pointers
    delete c0;
}

void plot_orbit_muons2(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, string output_dir){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    g->GetYaxis()->SetTitle("Luminosity HFOC [#times10^{34}cm^{-2}s^{-1}]");
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("Lumi section ([2^{18}] Orbits)");
    g->GetXaxis()->SetTitleOffset(1);
    
    g->SetMinimum(0.);

    g->Draw("ap");


    //c0->SetLogx();
    //c0->SetLogy();
    /*
    string tex_string_02 = "num points =  "+to_string(total_entrances);
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.25, 0.854, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();
    */
    string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    TString thetex4(tex_string.c_str());
    TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    tex1 = new TLatex(0.35 -0.05, 0.95, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    string tex_string_04 = "Run "+run_number+" (2018)";
    TString thetex5(tex_string_04.c_str());
    tex1 = new TLatex(0.73 -0.05, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    tex1 = new TLatex(0.25, 0.89, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    //string filepath = "./Plots/"+filename+".png";
    //string filepath2 = "./Plots/"+filename+".pdf";

    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;
}

void plot_orbit_muons_hfoc(float orbit_number[], float n_muons[], Int_t total_entrances, string output_dir){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    g->GetYaxis()->SetTitle("Luminosity HFOC [#times10^{34}cm^{-2}s^{-1}]");
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("Lumi section ([2^{18}] Orbits)");
    g->GetXaxis()->SetTitleOffset(1);
    
    g->SetMinimum(0.);

    g->Draw("ap");


    //c0->SetLogx();
    //c0->SetLogy();
    /*
    string tex_string_02 = "num points =  "+to_string(total_entrances);
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.25, 0.854, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();
    */
    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    string filepath = output_dir+"/lumiSection_rate_hfoc.png";
    string filepath2 = output_dir+"/lumiSection_rate_hfoc.pdf";

    //c0->SaveAs("./Plots/lumiSection_rate_hfoc.pdf");
    //c0->SaveAs("./Plots/lumiSection_rate_hfoc.png");
    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;
}

void plot_orbit_muons_scouting(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    if (khertz){
        g->GetYaxis()->SetTitle("Scouting rate [kHz]");
    } else{
        g->GetYaxis()->SetTitle("Scouting rate [Hz]");
    }
    
    
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("Lumi section ([2^{18}] Orbits)");
    g->GetXaxis()->SetTitleOffset(1);
    
    g->SetMinimum(0.);

    g->Draw("ap");


    //c0->SetLogx();
    //c0->SetLogy();
    /*
    string tex_string_02 = "num points =  "+to_string(total_entrances);
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.25, 0.854, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();
    */
    
    string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    TString thetex4(tex_string.c_str());
    TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    tex1 = new TLatex(0.35 -0.05, 0.95, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    string tex_string_04 = "Run " +run_number+ " (2018)";
    TString thetex5(tex_string_04.c_str());
    tex1 = new TLatex(0.73 -0.05, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    tex1 = new TLatex(0.25, 0.89, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;
}

void plot_residuals(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, string output_dir){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    //g->GetYaxis()->SetTitle("Log #sqrt{(p_{i}-f(i))^{2}}");
    g->GetYaxis()->SetTitle("(Scouting-Fit)/Fit");
    g->GetYaxis()->SetTitleOffset(1.5);
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("Luminosity [#times10^{34}cm^{-2}s^{-1}]");
    g->GetXaxis()->SetTitleOffset(1);
    
    //g->SetMinimum(0.);

    float max_h = orbit_number[0];

    for (int i=0; i<total_entrances; i++) {
        if (orbit_number[i] > max_h){
            max_h = orbit_number[i]; 
        } 
    }
   
    float lim_inf = 0.6;
    float lim_sup = max_h;
    
    TF1 *myfunc = new TF1("myfunc","[1]*x+[0]",lim_inf,lim_sup);
    g->Fit(myfunc, "r");

    TF1 *fit = g->GetFunction("myfunc");
    double e0 = fit->GetParError(0);
    double e1 = fit->GetParError(1);
    double p0 =fit->GetParameter(0);
    double p1 =fit->GetParameter(1);

    cout << " 4 " << endl;

    cout << " e0 = " << e0 << endl;
    cout << " e1 = " << e1 << endl;
    cout << " p0 = " << p0 << endl;
    cout << " p1 = " << p1 << endl;

    g->GetFunction("myfunc")->SetLineColor(kGreen);

    g->Draw("ap");

    //TLine *line = new TLine(gPad->GetUxmin(),gPad->GetUxmax(),orbit_number[total_entrances-1],0);
    gPad->Update();
    TLine *line = new TLine(gPad->GetUxmin(), 0, gPad->GetUxmax(), 0);
    gPad->Update();
    line->SetLineWidth(1);
    //line->SetHorizontal(kTRUE);
    line->SetLineColor(kBlack);
    line->SetLineStyle(2);
    line->Draw("same");

    g->Draw("psame");

    
    std::stringstream s_e0;
    s_e0 << std::fixed << std::setprecision(2) << e0;
    std::stringstream s_e1;
    s_e1 << std::fixed << std::setprecision(2) << e1;
    std::stringstream s_p0;
    s_p0 << std::fixed << std::setprecision(2) << p0;
    std::stringstream s_p1;
    s_p1 << std::fixed << std::setprecision(2) << p1;
    
    string tex_string_02 = "y =  ("+s_p1.str()+"#pm"+s_e1.str()+") x + ("+s_p0.str()+"#pm"+s_e0.str()+")";
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.20,  0.89-0.04*2, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();


    string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    TString thetex4(tex_string.c_str());
    //TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    tex1 = new TLatex(0.20, 0.89, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    string tex_string_04 = "Run " +run_number+ " (2018)";
    TString thetex5(tex_string_04.c_str());
    tex1 = new TLatex(0.73 -0.05, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    tex1 = new TLatex(0.20, 0.89-0.04, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");


    
    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");

    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;
}

void plot_residuals_cut(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, string output_dir){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 800, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kBlack);
    g->SetMarkerSize(0.5);
    //g->GetYaxis()->SetTitle("Log #sqrt{(p_{i}-f(i))^{2}}");
    g->GetYaxis()->SetTitle("(Scouting-Fit)/Fit");
    g->GetYaxis()->SetTitle("Deviation from linearity [%]");
    g->GetYaxis()->SetTitleOffset(1.5);
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("HFOC inst. luminosity [10^{34}cm^{-2}s^{-1}]");
    g->GetXaxis()->SetTitleOffset(1);
    
    //////g->SetMinimum(-0.04);
    //////g->SetMaximum(0.04);
    //////g->SetMinimum(-4);
    //////g->SetMaximum(4);

    //// quitar ticks arriba y derecha
    ////gPad->SetTickx(0); 
    ////gPad->SetTicky(0);

    float max_h = orbit_number[0];

    for (int i=0; i<total_entrances; i++) {
        if (orbit_number[i] > max_h){
            max_h = orbit_number[i]; 
        } 
    }
   
    //float lim_inf = 0.6;
    float lim_inf = orbit_number[0];
    float lim_sup = max_h;
    /*
    TF1 *myfunc = new TF1("myfunc","[1]*x+[0]",lim_inf,lim_sup);
    g->Fit(myfunc, "r");

    TF1 *fit = g->GetFunction("myfunc");
    double e0 = fit->GetParError(0);
    double e1 = fit->GetParError(1);
    double p0 =fit->GetParameter(0);
    double p1 =fit->GetParameter(1);

    cout << " 4 " << endl;

    cout << " e0 = " << e0 << endl;
    cout << " e1 = " << e1 << endl;
    cout << " p0 = " << p0 << endl;
    cout << " p1 = " << p1 << endl;

    g->GetFunction("myfunc")->SetLineColor(kBlack);
    //g->GetFunction("myfunc")->SetLineColor(0);
    */
    g->Draw("apE");

    //TLine *line = new TLine(gPad->GetUxmin(),gPad->GetUxmax(),orbit_number[total_entrances-1],0);
    gPad->Update();
    TLine *line = new TLine(gPad->GetUxmin(), 0, gPad->GetUxmax(), 0);
    gPad->Update();
    line->SetLineWidth(1);
    //line->SetHorizontal(kTRUE);
    line->SetLineColor(kBlack);
    line->SetLineStyle(2);
    line->Draw("same");
    //line->Draw("apE");

    //line = new TLine(gPad->GetUxmin(), 0.01, gPad->GetUxmax(), 0.01);
    line = new TLine(gPad->GetUxmin(), 1, gPad->GetUxmax(), 1);
    gPad->Update();
    line->SetLineWidth(1);
    //line->SetHorizontal(kTRUE);
    line->SetLineColor(kBlack);
    line->SetLineStyle(2);
    line->Draw("same");

    //line = new TLine(gPad->GetUxmin(), -0.01, gPad->GetUxmax(), -0.01);
    line = new TLine(gPad->GetUxmin(), -1, gPad->GetUxmax(), -1);
    gPad->Update();
    line->SetLineWidth(1);
    //line->SetHorizontal(kTRUE);
    line->SetLineColor(kBlack);
    line->SetLineStyle(2);
    line->Draw("same");

    g->Draw("psame");

    TLatex*tex1;
    /*
    std::stringstream s_e0;
    s_e0 << std::fixed << std::setprecision(4) << e0;
    std::stringstream s_e1;
    s_e1 << std::fixed << std::setprecision(4) << e1;
    std::stringstream s_p0;
    s_p0 << std::fixed << std::setprecision(4) << p0;
    std::stringstream s_p1;
    s_p1 << std::fixed << std::setprecision(4) << p1;
    
    string tex_string_02 = "y =  ("+s_p1.str()+"#pm"+s_e1.str()+") x + ("+s_p0.str()+"#pm"+s_e0.str()+")";
    TString thetex2(tex_string_02.c_str());
    
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.20,  0.89-0.04*2, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    //tex1->Draw();
    */

    //string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    string tex_string = "#bf{CMS}";
    TString thetex4(tex_string.c_str());
    //TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    tex1 = new TLatex(0.20, 0.87, thetex4);
    /////tex1 = new TLatex(0.20, 0.87-0.040, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    ////tex1->SetTextSize(0.045);
    tex1->SetTextSize(0.055);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    //string tex_string_04 = "Run " +run_number+ " (2018)";
    string tex_string_04 = "13 TeV (2018)";
    TString thetex5(tex_string_04.c_str());
    //tex1 = new TLatex(0.73 -0.10, 0.95, thetex5);
    tex1 = new TLatex(0.80 -0.10, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    ////tex1 = new TLatex(0.20, 0.89, thetex6);
    tex1 = new TLatex(0.20, 0.85-0.040, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    tex1->Draw("same");

    string tex_string_07 = "pp collisions, 13 TeV";
    TString thetex7(tex_string_07.c_str());
    tex1 = new TLatex(0.20, 0.95, thetex7);
    //tex1 = new TLatex(0.20, 0.89, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    //tex1->Draw("same");
    
    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;
}


void plot_scouting_v_hfoc(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    if (khertz){
        g->GetYaxis()->SetTitle("Scouting rate [kHz]");
    } else{
        g->GetYaxis()->SetTitle("Scouting rate [Hz]");
    }
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("HFOC luminosity[#times10^{34}cm^{-2}s^{-1}]");
    g->GetXaxis()->SetTitleOffset(1);
    
    g->SetMinimum(0.);

    g->Draw("ap");


    //c0->SetLogx();
    //c0->SetLogy();
    /*
    string tex_string_02 = "num points =  "+to_string(total_entrances);
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.25, 0.854, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();
    */
    
    TLatex*tex1;
    string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    TString thetex4(tex_string.c_str());
    //TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    tex1 = new TLatex(0.35 -0.05, 0.95, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    string tex_string_04 = "Run " +run_number+ " (2018)";
    TString thetex5(tex_string_04.c_str());
    tex1 = new TLatex(0.73 -0.05, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    tex1 = new TLatex(0.25, 0.89, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;
}

void plot_nfit(float orbit_number[], float n_muons[], Int_t total_entrances){
    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 800, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    g->GetYaxis()->SetTitle("Scouting rate [kHz]");
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("HFOC luminosity [#times10^{34}cm^{-2}s^{-1}]");
    g->GetXaxis()->SetTitleOffset(1);

    TGaxis::SetExponentOffset(-0.02, 0.0, "x");
    
    g->SetMinimum(0.);

    //Find max of ls array

    float max_h = orbit_number[0];

    for (int i=0; i<total_entrances; i++) {
        if (orbit_number[i] > max_h){
            max_h = orbit_number[i]; 
        } 
    }

   
    float lim_inf = 85000;
    float lim_sup = max_h;
    

    TF1 *myfunc = new TF1("myfunc","[1]*x+[0]",lim_inf,lim_sup);

    cout << " 3 " << endl;

    g->Fit(myfunc, "r");

    TF1 *fit = g->GetFunction("myfunc");
    double e0 = fit->GetParError(0);
    double e1 = fit->GetParError(1);
    double p0 =fit->GetParameter(0);
    double p1 =fit->GetParameter(1);

    cout << " 4 " << endl;

    cout << " e0 = " << e0 << endl;
    cout << " e1 = " << e1 << endl;
    cout << " p0 = " << p0 << endl;
    cout << " p1 = " << p1 << endl;

    g->Draw("ap");

    //c0->SetLogx();
    //c0->SetLogy();

    std::stringstream s_e0;
    s_e0 << std::fixed << std::setprecision(1) << e0;
    std::stringstream s_e1;
    s_e1 << std::fixed << std::setprecision(1) << e1;
    std::stringstream s_p0;
    s_p0 << std::fixed << std::setprecision(1) << p0;
    std::stringstream s_p1;
    s_p1 << std::fixed << std::setprecision(1) << p1;
    
    string tex_string_02 = "y =  "+s_p1.str()+"#pm"+s_e1.str()+" + "+s_p0.str()+"#pm"+s_e0.str();
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.25, 0.854, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();
    
    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    //gStyle->SetOptFit();
    
    c0->SaveAs("./Plots/lumi_rate_fitted.pdf");
    c0->SaveAs("./Plots/lumi_rate_fitted.png");

    //Delete all pointers
    delete c0;
}

std::vector<float> plot_nfit_vector(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir){

    std::vector<float> fit_results;

    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 800, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kBlack);
    g->SetMarkerSize(0.8);
    //g->GetYaxis()->SetTitle("Scouting rate [kHz]");
    
    if (khertz){
        g->GetYaxis()->SetTitle("Mean trigger object rate [kHz]");
    } else{
        g->GetYaxis()->SetTitle("Mean trigger object rate [Hz]");
    }
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("HFOC inst. luminosity [10^{34}cm^{-2}s^{-1}]");
    g->GetXaxis()->SetTitleOffset(1);

    TGaxis::SetExponentOffset(-0.02, 0.0, "x");
    
    ////////g->SetMinimum(80.);

    ////////g->GetXaxis()->SetLimits(0.576,0.872);
    
    g->Draw("ap");

    //Find max of ls array

    float max_h = -1;
    float min_h = 1000000;

    for (int i=0; i<total_entrances; i++) {
        if (orbit_number[i] > max_h){
            max_h = orbit_number[i]; 
        }
        if (orbit_number[i] < min_h){
            min_h = orbit_number[i]; 
        } 
    }

   
    ///float lim_inf = 0.589;
    float lim_inf = min_h;
    float lim_sup = max_h;
    

    TF1 *myfunc = new TF1("myfunc","[1]*x+[0]",lim_inf,lim_sup);

    cout << " 3 " << endl;

    g->Fit(myfunc, "r");

    TF1 *fit = g->GetFunction("myfunc");
    double e0 = fit->GetParError(0);
    double e1 = fit->GetParError(1);
    double p0 =fit->GetParameter(0);
    double p1 =fit->GetParameter(1);

    cout << " 4 " << endl;

    cout << " e0 = " << e0 << endl;
    cout << " e1 = " << e1 << endl;
    cout << " p0 = " << p0 << endl;
    cout << " p1 = " << p1 << endl;

    g->GetFunction("myfunc")->SetLineColor(kRed);
    //g->GetFunction("myfunc")->SetLineWidth(1);
    //g->GetFunction("myfunc")->Draw("ap");

    //fit->SetLineColor(kRed);
    //fit->Draw("psame");
    //fit->Draw();

    //Linea completa

    ////g->Draw("ap");

    float extrapolated_lim = 7.5;

    //TF1 *myfunc2 = new TF1("myfunc","[1]*x+[0]",0,lim_sup);
    TF1 *myfunc2 = new TF1("myfunc","[1]*x+[0]",0,extrapolated_lim);
    myfunc2->SetParameters(p0,p1);
    myfunc2->SetLineColor(kBlack);
    myfunc2->SetLineWidth(1);
    myfunc2->SetLineStyle(2);
    //myfunc2->Draw("psame");
    //myfunc2->Draw();
    ///////myfunc2->Draw("same");
    c0->Update();

    g->Draw("psame");

    //// quitar ticks arriba y derecha
    ////gPad->SetTickx(0); 
    ////gPad->SetTicky(0);

    //myfunc2->Draw("same");



    //c0->SetLogx();
    //c0->SetLogy();

    std::stringstream s_e0;
    s_e0 << std::fixed << std::setprecision(1) << e0;
    std::stringstream s_e1;
    s_e1 << std::fixed << std::setprecision(1) << e1;
    std::stringstream s_p0;
    s_p0 << std::fixed << std::setprecision(1) << p0;
    std::stringstream s_p1;
    s_p1 << std::fixed << std::setprecision(1) << p1;
    
    string tex_string_02 = "y =  ("+s_p1.str()+"#pm"+s_e1.str()+") x + ("+s_p0.str()+"#pm"+s_e0.str()+")";
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    //tex1 = new TLatex(0.20, 0.25, thetex2);
    tex1 = new TLatex(0.20, 0.81, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();

    float extrapolated_rate = p1*7.5+p0;
    std::stringstream s_er;
    s_er << std::fixed << std::setprecision(0) << extrapolated_rate;
    string tex_string_03 = "rate in 7.5 [#times10^{34}cm^{-2}s^{-1}]  =  "+s_er.str()+" Hz";
    TString thetex3(tex_string_03.c_str());
    TLatex*tex2;
    tex2 = new TLatex(0.20, 0.814-0.040, thetex3);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex2->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex2->SetTextSize(0.035);
    //tex2->Draw();
    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    //gStyle->SetOptFit();

    
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    
    string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    //string tex_string = "#bf{CMS}";
    TString thetex4(tex_string.c_str());
    //TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    //tex1 = new TLatex(0.20, 0.87, thetex4);
    tex1 = new TLatex(0.20, 0.95, thetex4);
    //tex1 = new TLatex(0.20, 0.87-0.040, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    //tex1->SetTextSize(0.075);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    //string tex_string_04 = "Run " +run_number+ " (2018)";
    string tex_string_04 = "13 TeV (2018)";
    TString thetex5(tex_string_04.c_str());
    //tex1 = new TLatex(0.73 -0.10, 0.95, thetex5);
    tex1 = new TLatex(0.80 -0.10, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    //tex1 = new TLatex(0.20, 0.85-0.040, thetex6);
    tex1 = new TLatex(0.20, 0.87, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    tex1->Draw("same");


    string tex_string_07 = "pp collisions, 13 TeV";
    TString thetex7(tex_string_07.c_str());
    tex1 = new TLatex(0.20, 0.95, thetex7);
    //tex1 = new TLatex(0.20, 0.89, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.055);
    //tex1->Draw("same");

    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());

    //Delete all pointers
    delete c0;


    fit_results.push_back(p0);
    fit_results.push_back(e0);
    fit_results.push_back(p1);
    fit_results.push_back(e1);

    return fit_results;  
}

std::vector<float> plot_nfit_vector_extended(float orbit_number[], float n_muons[], Int_t total_entrances, string filename, bool khertz, string output_dir){

    std::vector<float> fit_results;

    SetAtlasStyle(); 

    /* ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- 
     * ---- -- create a canvas where the histogram will be drawn -- ---
     * ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- */
    
    TCanvas *c0 = new TCanvas("c0", "First plot", 600, 600);
    //c0->SetGrid();
    c0->SetBorderMode(0);
    c0->SetFillColor(kWhite);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    //cout << "Uno"<<endl;

    TGraph *g = new TGraph(total_entrances,orbit_number,n_muons); //where x , y are 2 arrays of n points and draw this graph with option “p”
    
    g->SetMarkerStyle(kFullCircle);
    g->SetMarkerColor(kRed);
    g->SetMarkerSize(0.3);
    
    if (khertz){
        g->GetYaxis()->SetTitle("Scouting rate [kHz]");
    } else{
        g->GetYaxis()->SetTitle("Scouting rate [Hz]");
    }
    //g->GetXaxis()->SetTitle("Orbit number");
    g->GetXaxis()->SetTitle("HFOC luminosity  [#times10^{34}cm^{-2}s^{-1}]");
    g->GetXaxis()->SetTitleOffset(1);

    TGaxis::SetExponentOffset(-0.02, 0.0, "x");
    
    g->SetMinimum(0.);

    //Find max of ls array

    float max_h = orbit_number[0];

    for (int i=0; i<total_entrances; i++) {
        if (orbit_number[i] > max_h){
            max_h = orbit_number[i]; 
        } 
    }

   
    float lim_inf = 0.6;
    float lim_sup = max_h;
    

    TF1 *myfunc = new TF1("myfunc","[1]*x+[0]",lim_inf,lim_sup);

    cout << " 3 " << endl;

    g->Fit(myfunc, "r");

    TF1 *fit = g->GetFunction("myfunc");
    double e0 = fit->GetParError(0);
    double e1 = fit->GetParError(1);
    double p0 =fit->GetParameter(0);
    double p1 =fit->GetParameter(1);

    cout << " 4 " << endl;

    cout << " e0 = " << e0 << endl;
    cout << " e1 = " << e1 << endl;
    cout << " p0 = " << p0 << endl;
    cout << " p1 = " << p1 << endl;

    g->GetFunction("myfunc")->SetLineColor(kGreen);

    //Linea completa

    //g->Draw("ap");

    float extrapolated_lim = 8;

    //TF1 *myfunc2 = new TF1("myfunc","[1]*x+[0]",0,lim_sup);
    TF1 *myfunc2 = new TF1("myfunc","[1]*x+[0]",0,extrapolated_lim);
    myfunc2->SetParameters(p0,p1);
    myfunc2->SetLineColor(kBlack);
    myfunc2->SetLineWidth(2);
    myfunc2->SetLineStyle(2);
    //myfunc2->Draw("same");
    myfunc2->Draw();
    
    if (khertz){
        myfunc2->GetYaxis()->SetTitle("Scouting rate [kHz]");
    } else{
        myfunc2->GetYaxis()->SetTitle("Scouting rate [Hz]");
    }
    //g->GetXaxis()->SetTitle("Orbit number");
    myfunc2->GetXaxis()->SetTitle("HFOC luminosity [#times10^{34}cm^{-2}s^{-1}]");
    myfunc2->GetXaxis()->SetTitleOffset(1);
    //c0->Update();

    g->Draw("psame");

    //myfunc2->Draw("same");



    //c0->SetLogx();
    //c0->SetLogy();

    std::stringstream s_e0;
    s_e0 << std::fixed << std::setprecision(0) << e0;
    std::stringstream s_e1;
    s_e1 << std::fixed << std::setprecision(0) << e1;
    std::stringstream s_p0;
    s_p0 << std::fixed << std::setprecision(0) << p0;
    std::stringstream s_p1;
    s_p1 << std::fixed << std::setprecision(0) << p1;
    
    string tex_string_02 = "y =  ("+s_p1.str()+"#pm"+s_e1.str()+") x +("+s_p0.str()+"#pm"+s_e0.str()+")";
    TString thetex2(tex_string_02.c_str());
    TLatex*tex1;
    //tex1 = new TLatex(0.34, 0.954, thetex2);
    tex1 = new TLatex(0.20, 0.854-0.040, thetex2);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw();

    float extrapolated_rate = p1*7.5+p0;
    std::stringstream s_er;
    s_er << std::fixed << std::setprecision(0) << extrapolated_rate;
    string tex_string_03 = "rate in 7.5 [#times10^{34}cm^{-2}s^{-1}]  =  "+s_er.str()+" Hz";
    TString thetex3(tex_string_03.c_str());
    TLatex*tex2;
    tex2 = new TLatex(0.20, 0.814-0.040, thetex3);
    //tex1 = new TLatex(0.40 -0.05, 0.954, thetex2);
    tex2->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex2->SetTextSize(0.035);
    tex2->Draw();


    string tex_string = "#bf{CMS} #font[52]{Preliminary} ";
    TString thetex4(tex_string.c_str());
    //TLatex*tex1;
    //TString thetex4(tex_string.c_str());
    // TLatex*tex1;
    tex1 = new TLatex(0.20, 0.89, thetex4);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.045);
    //tex1 = new TLatex(0.15 -0.05, 0.95, thetex2);
    //tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    //string tex_string_04 = "pp, #sqrt{s} = 13 TeV";
    string tex_string_04 = "Run " +run_number+ " (2018)";
    TString thetex5(tex_string_04.c_str());
    tex1 = new TLatex(0.73 -0.05, 0.95, thetex5);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");

    string tex_string_06 = "#bf{Endcap Muon Track Finder (EMTF)}";
    TString thetex6(tex_string_06.c_str());
    tex1 = new TLatex(0.20, 0.89-0.040, thetex6);
    tex1->SetNDC(kTRUE);
    // tex1->SetTextFont(42);
    tex1->SetTextSize(0.035);
    tex1->Draw("same");


    /* ---- ---- ---- ---- ---- ---- ---
       ---- Save canvas into a file ----
       ---- ---- ---- ---- ---- ---- --- */
    
    //c0->SaveAs("./Plots/fit_error.pdf");
    //gStyle->SetOptFit();
    
    string filepath = output_dir+"/"+filename+".png";
    string filepath2 = output_dir+"/"+filename+".pdf";

    c0->SaveAs(filepath.c_str());
    c0->SaveAs(filepath2.c_str());


    //Delete all pointers
    delete c0;


    fit_results.push_back(p0);
    fit_results.push_back(e0);
    fit_results.push_back(p1);
    fit_results.push_back(e1);

    return fit_results;  
}


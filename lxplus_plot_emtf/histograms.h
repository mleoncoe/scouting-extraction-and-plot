#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include "TROOT.h"
#include "TTree.h"
#include "TH2.h"
#include "TH1.h"
#include "TString.h"
#include "TLegend.h"
#include "TSystem.h"
#include "TAxis.h"
#include "TLatex.h"
#include "TLorentzVector.h"
#include "TMinuit.h"
#include "ATLAS/AtlasStyle.h"
#include "ATLAS/AtlasUtils.h"
#include "ATLAS/AtlasLabels.h"
#include "TFile.h"            
#include "TCanvas.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TF1.h"
#include "TF2.h"
#include "TStyle.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TSlider.h"
#include <iomanip>
#include <math.h>
#include <cmath>
#include "TGaxis.h"

using namespace std;
//using namespace RooFit;

//void plot(int numb_evts, int numb_bins);

void plot();

//int main(/*int argc, char* argv[]*/);
int main(int argc, char* argv[]);

import scipy.stats
import json
import os
import bz2
import csv
from timeit import default_timer as timer
import array
import argparse
import numpy as np
import re
import operator
from MasksShiftsScales import MuonMask, MuonMaskShift, HeaderMask, HeaderShift, BxMask, BxMaskShift, GMTScales, BlockMask
from RunInfo_117_5 import Run


parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()

group.add_argument("-o",   "--console", type=int, help="number of muons written to console")
group.add_argument("-r",   "--rate_file", action="store_true", default=False, help="produce rate file")
parser.add_argument("-csv", "--csv_file", action="store_true", help="write csv file")
#parser.add_argument("-c",   "--cut", type=float, help="apply pt cut: pt >= cut")
parser.add_argument("-c",   "--cut", help="apply cut, for example: pt>cut")
parser.add_argument("-run",   "--run_number", choices=['325117', '325097', '325159', '325174', '325022','325099'], required=True, help="run number")

args = parser.parse_args()
TO_CONSOLE = bool(args.console)
NUM_MUONS_PRINTED = args.console


INPUT_DIR = '/eos/cms/store/cmst3/group/daql1scout/run2'
INPUT_ENDING = '.dat.bz2'
OUTPUT_DIR = '/eos/user/m/mleoncoe/Scouting'
OUTPUT_CSV_FILE = 'muons_117_5.csv'
OUTPUT_RATE_FILE = 'rate_file.json'
 
BATCH_SIZE = int(1e7)	# Size of batches read at once in bytes 
BX_CORRECTION = -8
ORBIT_TIME =  88.92e-6

if args.cut:
	cut_operator = False
	op_dict1 = {">=": operator.ge, "<=": operator.le}
	op_dict2 = {">": operator.gt, "<": operator.lt, "==": operator.eq}
	for key in op_dict1.keys():
		if key in args.cut:
			cut_variable, cut_value	= re.split(key, args.cut)
			cut_value = float(cut_value)
			cut_operator = op_dict1[key]
			break
	if not cut_operator:
		for key in op_dict2.keys():
			if key in args.cut:
				cut_variable, cut_value	= re.split(key, args.cut)
				cut_value = float(cut_value)
				cut_operator = op_dict2[key]
				break
	if not cut_operator:
		raise ValueError("cut argument has to be of the form variable [>,<,>=,<=] value")
	#op = re.findall(r"[(>=),(<=)]", args.cut)
	#print(op)
	#if len(op) != 1:
	#	raise ValueError("cut argument has to be of the form variable [>,<,>=,<=] value")
	#cut_operator = op[0]
	#cut_variable, cut_value	= re.split(cut_operator, args.cut)

end_print = False
counter = 0
start_timer = 0 
end_timer = 0

run = Run(args.run_number)

if args.rate_file:
	fill_dict = {"Scan_1":[], "Scan_2":[]}		# dictionary containing all rates and rateerrors -> to json file	
	rate_array = np.zeros((18, 3655))		

	bx_list = [x for x in range(3655)]
	bx_list = list(map(str, bx_list))
	
	err_list = [[] for i in range(18)]
	step_data_list = [ [] for i in range(3655) ]
		
	current_scan_point = False
	first = True

	scan = run.scan
	NULL_ORBIT = run.NULL_ORBIT

	NULL_TS = round(run.FIRST_TS + NULL_ORBIT * ORBIT_TIME + run.DOWNTIME_CORRECTION + run.DAQ_CORRECTION)	#match orbitnumber and timestamps
	orbits_in_step = [(x[1]-x[0])/ORBIT_TIME for x in scan]
	SCAN_BEGIN_TS = scan[0][0]
	SCAN_END_TS = scan[-1][1]	
	steps_per_scan = int(len(scan)/2)	

	
# reading and calculating muon variables: needed for cuts
class Muon:
	def __init__(self, muon, bx, orbit):
		self.orbit = orbit
		self.bx = bx
		self.decode_binary_muon_data(muon)
		self.apply_scales()
	
	def decode_bx(self, binary_bx):		# not used
		#if binary_bx > 0xf0000000: binary_bx -= 0xf0000000
		self.bx = (binary_bx & BxMask.bx) >> BxMaskShift.bx
		if self.bx >= 3655: self.bx %= 3654
		self.interm = (binary_bx & BxMask.interm) >> BxMaskShift.interm
		self.linkid = (binary_bx & BxMask.linkid) >> BxMaskShift.linkid        
	
	def decode_binary_muon_data(self, binary_muon):
		variables = [attr for attr in dir(MuonMask) if not attr.startswith("__")]
		for var in variables:
			mask = getattr(MuonMask, var)
			shift = getattr(MuonMaskShift, var)
			setattr(self, var, (binary_muon & mask) >> shift)
	
	def apply_scales(self):
		self.pt = (self.pt - 1) * GMTScales.pt_scale   # in C File: pt-1, why??
		self.phi *= GMTScales.phi_scale
		self.eta *= GMTScales.eta_scale
		self.phiext *= GMTScales.phi_scale
		self.etaext *= GMTScales.eta_scale


# print muon variables to console
def write_to_console(muons):
	for muon in muons:
		print('{:>4d}{:>6d}{:>7.1f}{:>11.5f}{:>11.5f}{:>5d}{:>8d}{:>7d}{:>9d}{:>13.5f}{:>11.5f}{:>6d}'.format( \
		muon.orbit, muon.bx, muon.pt, muon.eta, muon.phi, muon.iso, muon.chrg, muon.chrgv, muon.index, muon.phiext, muon.etaext, muon.qual))


# read muons from block of binary data
def make_muons(muon_block, num, bx, block_orbit):
	muons = []
	for i in range(num):
		muon = muon_block[2*i:2*i+2]
		muon = (muon[0] << 32) |  muon[1]
		muons.append(Muon(muon, bx, block_orbit))
	return muons	


# get rates and rate errors per bx
def make_statistics(timestamp, bx, num):
	global first, current_scan_point, step_data_list
	scan_point_bool = [(step[0] <= timestamp and timestamp < step[1]) for step in scan] 	
	if np.any(scan_point_bool):
		scan_point = [i for i, val in enumerate(scan_point_bool) if val][0]	
		rate_array[scan_point, bx] += num 
			
		if first:
			current_scan_point = scan_point
			first = False
			print("processing first step of scan")	
		if scan_point == current_scan_point:
			step_data_list[bx].append(num)
		else:				
			err_list[current_scan_point] = [scipy.stats.sem(step_data) for step_data in step_data_list]
			if not first:
				write_rate_file()				
			current_scan_point = scan_point
			step_data_list = [ [] for i in range(3655) ]
			print('processing step', scan_point+1)	
			step_data_list[bx].append(num)


def get_timestamp(orbit, bx, num):
	delta_orbit = orbit - NULL_ORBIT
	timestamp = round(NULL_TS + delta_orbit * ORBIT_TIME)

	if timestamp >= SCAN_BEGIN_TS and timestamp < SCAN_END_TS:
		if args.rate_file:
			make_statistics(timestamp, bx, num)

	return timestamp


# remove NaN from rates and rate errors (non valid bx)
def clean_dict():
	global fill_dict

	unvalid_bx_list = set()
	for step in range(steps_per_scan):
		for bx, err in fill_dict["Scan_1"][step]["RateErrs"].items():
			if np.isnan(err):
				unvalid_bx_list.add(bx)
		for bx, err in fill_dict["Scan_2"][step]["RateErrs"].items():
			if np.isnan(err):
				unvalid_bx_list.add(bx)
	for bx in unvalid_bx_list:	
		for step in range(steps_per_scan):
			if fill_dict["Scan_1"][step]["RateErrs"]:
				del fill_dict["Scan_1"][step]["RateErrs"][bx]	
			if fill_dict["Scan_1"][step]["Rates"]:
				del fill_dict["Scan_1"][step]["Rates"][bx]	
			if fill_dict["Scan_2"][step]["RateErrs"]:
				del fill_dict["Scan_2"][step]["RateErrs"][bx]	
			if fill_dict["Scan_2"][step]["Rates"]:
				del fill_dict["Scan_2"][step]["Rates"][bx]


def write_rate_file():
	global fill_dict
	
	fill_dict = {"Scan_1":[], "Scan_2":[]}	
	
	for step in range(steps_per_scan):
		fill_dict["Scan_1"].append({"RateErrs":dict(zip(bx_list, err_list[step])),  
					   "Rates":dict(zip(bx_list, rate_array[step,:]/orbits_in_step[step])), 
					   "ScanName":"X1", 
					   "ScanNumber":1, 
					   "ScanPoint":step+1})	
		fill_dict["Scan_2"].append({"RateErrs":dict(zip(bx_list, err_list[step+9])), 
					   "Rates":dict(zip(bx_list, rate_array[step+9,:]/orbits_in_step[step+9])), 
					   "ScanName":"Y1", 
					   "ScanNumber":2, 
					   "ScanPoint":step+1})		
	clean_dict()		
	with open(OUTPUT_RATE_FILE, 'w') as json_file:
		json.dump(fill_dict, json_file)			

	
# measure elapsed time
def measure_time(count):
	global start_timer, end_timer
	if count == 0: 
		start_timer = 0
	end_timer = timer()
	if not TO_CONSOLE:
		print(count, "blocks,  ", end_timer - start_timer, "s")	
	start_timer = timer()


# write csv file with timestamps
def write_csv_ts_file(timestamp, orbit, muons_in_orbit):
	global csv_writer
	if args.rate_file:
		if timestamp >= SCAN_BEGIN_TS and timestamp <= SCAN_END_TS:	
			csv_writer.writerow([orbit, muons_in_orbit, timestamp])	

def write_csv_file(orbit, muons_in_orbit):
	csv_writer.writerow([orbit, muons_in_orbit])

def write_csv_file_allInfo(orbit, muons_in_orbit, timestamp, bx_number):
	csv_writer.writerow([orbit, muons_in_orbit, timestamp, bx_number])



# process batch of binary data consisting of blocks of varying size: read header of block -> number of muons -> size of block -> jump to next header
def process_batch(batch, orbit, muons_in_orbit, count):
	global end_print, counter

	first_header = batch[0]		
	idx_counter = ((first_header & HeaderMask.mAcount) >> HeaderShift.mAcount) + ((first_header & HeaderMask.mBcount) >> HeaderShift.mBcount)
	muons_in_obit = idx_counter
	header_idx = 0	
	batch_counter = 1
	num = 0
	batch_len = len(batch)
	
	while batch_len >= header_idx + num*2 + 38:	# 38 due to maximum size of next block
		if end_print: break
		
		header_idx = 3*batch_counter + 2*idx_counter
		header = batch[header_idx]	
			
		block_orbit = batch[header_idx + 2]	       						
		num = ((header & HeaderMask.mAcount) >> HeaderShift.mAcount) + ((header & HeaderMask.mBcount) >> HeaderShift.mBcount)
				
		bx = (batch[header_idx+1] & BxMask.bx) >> BxMaskShift.bx
		bx += BX_CORRECTION
		if bx >= 3655: 
			bx %= 3655		
		
		if args.cut or TO_CONSOLE:
			muons = make_muons(batch[header_idx+3:header_idx+3+2*num], num, bx, block_orbit)
		if args.cut:
			#print(cut_operator(getattr(muons[0], cut_variable), cut_value))	
			muons = [mu for mu in muons if cut_operator(getattr(mu, cut_variable), cut_value)]
			#muons = [mu for mu in muons if mu.pt >= args.cut]
			num_after_cut = len(muons)
		else: 
			num_after_cut = num
		
		if args.rate_file:	
			timestamp = get_timestamp(block_orbit, bx, num_after_cut)
			if timestamp > SCAN_END_TS:					
				err_list[current_scan_point] = [scipy.stats.sem(step_data) for step_data in step_data_list]
				write_rate_file()
				print(timestamp)
				print("End of Scan.")
				raise SystemExit
		if TO_CONSOLE:
			write_to_console(muons)
			counter += num_after_cut
			if counter >= NUM_MUONS_PRINTED:
				end_print = True 	
			
		if count == 0: 
			orbit = block_orbit
		
		if orbit != block_orbit:
			if args.csv_file and args.rate_file:
				write_csv_ts_file(timestamp, orbit, muons_in_orbit)	
			elif args.csv_file:
				write_csv_file(orbit, muons_in_orbit)
				#timestamp = get_timestamp(block_orbit, bx, num_after_cut)
				#write_csv_file_allInfo(orbit, muons_in_orbit,timestamp,bx)
			muons_in_orbit = 0
		
		if count%10000000 == 0: 
			measure_time(count)
			
		orbit = block_orbit		
		muons_in_orbit += num_after_cut	
		idx_counter += num
		
		count += 1
		batch_counter += 1
	
	residual = batch[header_idx+1+(num+1)*2:]
	return residual, orbit, muons_in_orbit, count
#TODO: take care of the last residual of the last batch!!	


# read batches of data SE EJECUTA POR CADA FILE
def read_bin_data(inputf, orbit, muons_in_orbit, count):			
	residual = array.array("I")
	
	while 1:
		byte_array = array.array("I") 
		batch = inputf.read(BATCH_SIZE)
		if not batch: break
		byte_array.fromstring(batch)
		byte_array = residual + byte_array	
		if end_print: break	
		residual, orbit, muons_in_orbit, count = process_batch(byte_array, orbit, muons_in_orbit, count)	
	return orbit, muons_in_orbit, count


def open_zip_files(first_file, last_file):
	orbit = -1
	muons_in_orbit = 0	
	count = 0

	for file_num in range(first_file, last_file+1):	
		if end_print: 
			break
		if not TO_CONSOLE:
			print('Processing file '+str(file_num))
		if len(str(file_num)) == 2:
			string_file_num = str(file_num)
		elif len(str(file_num)) == 1:
			string_file_num = "0" + str(file_num)
		input_path = os.path.join(INPUT_DIR, run.file_name + string_file_num + INPUT_ENDING)
		bz_file = bz2.BZ2File(input_path, mode='rb')
		orbit, muons_in_orbit, count = read_bin_data(bz_file, orbit, muons_in_orbit, count)


if TO_CONSOLE:
	counter = 0
	num_printed = 30
	print('{:>4s}{:>9s}{:>7s}{:>8s}{:>11s}{:>11s}{:>9s}{:>8s}{:>7s}{:>10s}{:>11s}{:>9s}'.format( \
	      'orbit', 'bx', 'pt', 'eta', 'phi', 'iso', 'chrg', 'chrgv', 'index', 'phiext', 'etaext', 'qual'))	
	# TODO: improve print statement

output_path = os.path.join(OUTPUT_DIR, OUTPUT_CSV_FILE)
with open(output_path, 'w') as outputf:		
	csv_writer = csv.writer(outputf, delimiter=',')
	open_zip_files(run.first_file, run.last_file)





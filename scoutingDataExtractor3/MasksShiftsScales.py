import math


class BxMaskShift:
	bx      = 0	# 0
	interm  = 28	# original: 27 and 23??
	linkid  = 24 	#

class BxMask:
	bx      = 0xffff << BxMaskShift.bx
	interm  = 0x000f << BxMaskShift.interm
	linkid  = 0x000f << BxMaskShift.linkid


class MuonMaskShift:
	phiext =  32	#  0
	pt     =  42	# 10
	qual   =  51	# 19
	etaext =  55	# 23
	iso    =   0	#  0
	chrg   =   2	#  2
	chrgv  =   3	#  3
	index  =   4	#  4
	phi    =  11	# 11
	eta    =  21	# 21
	rsv    =  30	# 30

class MuonMask:
	phiext  = 0x03ff << MuonMaskShift.phiext 	# in C-Code: 0x01ff FEHLER?!!
	pt      = 0x01ff << MuonMaskShift.pt
	qual    = 0x000f << MuonMaskShift.qual
	etaext  = 0x01ff << MuonMaskShift.etaext
	iso     = 0x0003 << MuonMaskShift.iso
	chrg    = 0x0001 << MuonMaskShift.chrg
	chrgv   = 0x0001 << MuonMaskShift.chrgv
	index   = 0x007f << MuonMaskShift.index
	phi     = 0x03ff << MuonMaskShift.phi
	eta     = 0x01ff << MuonMaskShift.eta
	rsv     = 0x0003 << MuonMaskShift.rsv
	#etaextv = 0x00ff
	#etav    = 0x00ff
	#etas    = 0x0100
	#etaexts = 0x0100
	#+2reservedbits


class HeaderShift:
	bx_match    = 24
	mAcount     = 16
	orbit_match =  8
	mBcount     =  0

class HeaderMask:
	bx_match    = 0xff << HeaderShift.bx_match
	mAcount     = 0x0f << HeaderShift.mAcount
	orbit_match = 0xff << HeaderShift.orbit_match
	mBcount     = 0x0f << HeaderShift.mBcount


class GMTScales:
	pt_scale  = 0.5
	phi_scale = 2.* math.pi / 576.
	eta_scale = 0.0870/8;	# 9th MS bit is sign
	phi_range = math.pi


class BlockMask:
        bx = 0xffff
        orbit = 0xffff
        muon = 0xffffffff




